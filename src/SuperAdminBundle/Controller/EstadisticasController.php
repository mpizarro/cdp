<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 10-07-17
 * Time: 12:00
 */

namespace SuperAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EstadisticasController extends Controller
{

    public function viewEstadisticasAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('estadisticas');

        return $this->render('SuperAdminBundle::ver_usuarios.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user']
            ));
    }

}