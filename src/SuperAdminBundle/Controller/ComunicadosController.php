<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 10-07-17
 * Time: 15:33
 */

namespace SuperAdminBundle\Controller;


use CoreBundle\Entity\Comunicados;
use CoreBundle\Form\ComunicadosType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ComunicadosController extends Controller
{
    public function viewComunicadosAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('ver_comunicados');

        return $this->render('SuperAdminBundle::ver_usuarios.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],

            ));
    }

}