<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 10-07-17
 * Time: 12:00
 */

namespace SuperAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcher;

class UsuariosController extends Controller
{
    /**
     * Metodo que carga vista de Ver Usuarios
     *  Extrae la lista de usuarios filtrado por rol para crear tabla:
     *      ROLE_PROFESOR
     *      ROLE_SOPORTE
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewUsersAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('ver_usuarios');
        $array_profesores = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"ROLE_PROFESOR"%')->getQuery()->getResult();

        $array_soportes = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"ROLE_SOPORTE"%')->getQuery()->getResult();

        $profesores = array();
        $soportes   = array();


        $c= 0;
        foreach ($array_profesores as $item) {
            if (!$item->hasRole('ROLE_SOPORTE')){
                $profesores[$c]['nombre']   =   $item->getUsername();
                $profesores[$c]['email']    =   $item->getEmail();
                $profesores[$c]['activo']   =   $item->isEnabled();
                $profesores[$c]['id']       =   $item->getId();
                $c++;
            }
        }
        $c = 0;
        foreach ($array_soportes as $item) {
            $soportes[$c]['nombre'] =   $item->getUsername();
            $soportes[$c]['email']  =   $item->getEmail();
            $soportes[$c]['activo'] =   $item->isEnabled();
            $soportes[$c]['id']     =   $item->getId();
            $c++;
        }

        return $this->render('SuperAdminBundle::ver_usuarios.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
                'tabla'=>array('soportes'=>$soportes,'profesores'=>$profesores)
            ));
    }

}