<?php

namespace SuperAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('dashboard');

        return $this->render('CoreBundle::menubars.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user']
            ));
    }
}
