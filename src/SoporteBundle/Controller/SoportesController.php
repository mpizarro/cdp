<?php

namespace SoporteBundle\Controller;
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 17-07-17
 * Time: 11:44
 */

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SoportesController extends Controller
{
    public function indexAction()
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSoporte('soporte_dashboard');

        return $this->render('CoreBundle::menubars.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
            ));

    }

    public function viewProfesoresAction()
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSoporte('ver_profesores');

        return $this->render('@Soporte/ver_profesores.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
            ));

    }

    public function viewAsignacionesAction()
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSoporte('ver_asignaciones');

        return $this->render('SoporteBundle::ver_asignaciones.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
            ));

    }
}