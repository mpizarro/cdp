<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-06-17
 * Time: 21:55
 */

namespace ProfesorBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class DashboardController extends Controller
{
    public function indexAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('dashboard');
        $user = $this->getUser();

        return $this->render('ProfesorBundle::dashboard.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user']
        ));
    }
}