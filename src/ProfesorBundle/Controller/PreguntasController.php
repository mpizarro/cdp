<?php

namespace ProfesorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PreguntasController extends Controller {

    public function newPreguntaAction(Request $request) {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('nueva_pregunta');
        $user = $this->getUser();

        return $this->render('ProfesorBundle::crear_preguntas.html.twig', array('menu' => $menu['menu'],
                    'user' => $menu['user']
        ));
    }

    public function viewBancoPreguntasAction(Request $request) {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('banco_preguntas');

        return $this->render('ProfesorBundle::preguntas.html.twig', array('menu' => $menu['menu'],
                    'user' => $menu['user']
        ));
    }
}
