<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-06-17
 * Time: 21:55
 */

namespace ProfesorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PruebasController extends Controller
{

    public function newPruebaAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('nueva_prueba');
        $user = $this->getUser();

        return $this->render('ProfesorBundle::nueva_prueba.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user']
            ));
    }
    public function newFilaAction(Request $request, $codigo=null)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('nueva_prueba');
        $codigo = substr($codigo,1);
        return $this->render('ProfesorBundle::prueba_nueva_fila.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
                'codigo'=>print_r($codigo,true)
            ));
    }
    public function viewPruebasAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuUsuario('pruebas');

        return $this->render('ProfesorBundle::pruebas.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user']
            ));
    }

}