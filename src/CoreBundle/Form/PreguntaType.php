<?php

namespace CoreBundle\Form;

use CoreBundle\Entity\Pregunta;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreguntaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tipo_prueba',EntityType::class, array('class'=>'CoreBundle\Entity\TipoPrueba', 'label'=>false))
            ->add('nivel', EntityType::class, array('class'=>'CoreBundle\Entity\Nivel', 'label'=>false))
            ->add('subsector', EntityType::class, array('class'=>'CoreBundle\Entity\Subsector', 'label'=>false))
            ->add('habilidad', EntityType::class, array('class'=>'CoreBundle\Entity\Habilidad', 'label'=>false, 'required'=>false, 'empty_value'   => '(Opcional)'))
            ->add('eje', EntityType::class, array('class'=>'CoreBundle\Entity\Eje', 'label'=>false, 'required'=>false, 'empty_value'   => '(Opcional)'))
            ->add('taxonomia', EntityType::class, array('class'=>'CoreBundle\Entity\Taxonomia', 'label'=>false, 'required'=>false, 'empty_value'   => '(Opcional)'))
            ->add('subsector', EntityType::class, array('class'=>'CoreBundle\Entity\Subsector', 'label'=>false))
            ->add('dificultad', EntityType::class, array('class'=>'CoreBundle\Entity\Dificultad', 'label'=>false, 'required'=>false, 'empty_value'   => '(Opcional)'))

            ->add('publicado', ChoiceType::class, array(
                'label'=>false,
                'choices'=>array(
                    true,
                    false
                ),

                'choice_label' => function ($value, $key, $index) {
                    if ($value == true) {
                        return 'Publicar pregunta (será visible para todos y podra agregarla a una prueba).';
                    }else{
                        return 'Guardar como borrador (solo tu la podras ver).';
                    }
                },

                'choice_attr' => function($val, $key, $index) {
                    if ($val == false) {
                        return array('checked' => true);
                    }
                    return array('checked' => true);
                },

                'expanded'=>true,
                'multiple'=>false,
                'attr'=>array('class'=>'form-check-input')
            ))

            ->add('contenido', TextareaType::class, array('label'=>false))
            ->add('Guardar',SubmitType::class, array('attr'=>array(
                'class'=>'btn btn-success col-md-offset-1 col-md-3'
            )));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Pregunta::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pregunta_create';
    }

}
