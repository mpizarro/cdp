<?php

/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-07-17
 * Time: 15:44
 */
namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType

{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder->add('nombre');
        $builder->add('apellido');
        $builder->add('rol',
            HiddenType::class,
            array('mapped' => false));
    }

    public function getParent()

    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()

    {
        return 'app_user_registration';
    }

    public function getName()

    {
        return $this->getBlockPrefix();
    }
}

