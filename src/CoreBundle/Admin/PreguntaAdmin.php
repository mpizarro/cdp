<?php

namespace CoreBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PreguntaAdmin extends AbstractAdmin
{
    // Ventana Editar/Crear
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper


            ->add('contenido')

            ->add('publicado')
            ->add('taxonomia')
            ->add('dificultad')
            ->add('habilidad')

        ;
    }


    // Configuración de Filtros
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('codigo')
            ->add('tipoPrueba')
            ->add('nivel')
            ->add('subsector')
        ;
    }

    // Ventana listar
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('codigo', null, array(
                'route'=> array('name'=>'show')
            ))
            ->add('tipo_prueba')
            ->add('nivel')
            ->add('subsector')
            ->add('publicado')
            ->add('usuario.nombre', null, array('label'=>'Creador'))
        ;
    }


    // Pangtalla Ver
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Datos',
                array(
                    'class'       => 'col-md-6',
                    'box_class'   => 'box box-solid box-success'))
                ->add('nivel')
                ->add('tipo_prueba')
                ->add('subsector')
                ->add('dificultad')
                ->add('habilidad')
                ->add('eje')
                ->add('taxonomia')
            ->end()
            ->with('Pregunta y Respuestas',
                array(
                    'class'       => 'col-md-6',
                    'box_class'   => 'box box-solid box-success'))
                ->add('contenido','html', array('label'=>'Pregunta'))
            ->end()
        ;
    }

    // Rutas y botones
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('view',$this->getRouterIdParameter().'/view')
            ->remove('export') // Remueve el boton exportar
        ;

    }


}