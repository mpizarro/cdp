<?php

namespace CoreBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;

class UsuariosAdmin extends AbstractAdmin
{
    // Ventana Editar/Crear
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper

            ->with('Datos Generales', array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid box-primary',
                'description' => 'Datos generales del usuario'))

                ->add('username', 'text', array(
                    'label' => 'RUT'
                ))
                ->add('nombre', 'text', array(
                    'label' => 'Nombres'
                ))
                ->add('apellido', 'text', array(
                    'label' => 'Apellidos'
                ))
                ->add('email', 'text', array(
                    'label' => 'Email'
                ))
                ->add('email', 'email', array(
                    'label' => 'Email'
                ));
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $role = $securityContext->isGranted("ROLE_SUPER_ADMIN");
        if ($role){
            $formMapper->end();
            $formMapper->with("Seguridad",
                array(
                    'class'       => 'col-md-6',
                    'box_class'   => 'box box-solid box-danger',
                    'description' => 'Datos relativos a la seguridad del usuario'))
                ->add('perfil', 'choice', array(
                    'label'=>'Perfil',
                    'choices'=>array('ROLE_SOPORTE'=>'Soporte',
                        'ROLE_PROFESOR'=>'Profesor'),
                    'mapped'=>false
                ))
                ->add('colegio', null, array(
                    'label' => 'Colegio'
                ))
                ->end();

        }else{

            $formMapper->end()
                ->with("Colegio",
                    array(
                        'class'       => 'col-md-6',
                        'box_class'   => 'box box-solid box-danger',
                        'description' => 'Datos relativos al rol'))

                ->add('colegio', null, array(
                'label' => 'Colegio'
            ))
                ->add('subsectors', 'sonata_type_model', array(
                    'label' => 'Asignaturas',
                    'multiple'=>true,
                    'by_reference'=> false
                ))
                ->add('perfil', 'hidden', array(
                    'attr'=>array(
                        'hidden'=>true,
                        'value'=>'ROLE_PROFESOR'
                    ),
                    'mapped'=>false
                ))
                ->end();
        }
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $rol_selected = $this->getForm()->get('perfil')->getData();
        if($rol_selected == 'ROLE_PROFESOR'){
            $errorElement
                ->with('colegio')
                ->assertNotNull(array())
                ->end();
        }
    }

    public function prePersist($user)
    {
        $username = $user->getUsername();
        $password = substr($username,0,5);
        $user->setPlainPassword($password);
        $rol_selected = $this->getForm()->get('perfil')->getData();
        $user->addRole($rol_selected);
        $this->getUserManager()->updateUser($user);
    }

    // Configuración de Filtros
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, array(
                'label'=> "Rut"
            ))
            ->add('nombre')
            ->add('apellido')
            ->add('colegio', null, array('label'=>'Colegio'))
        ;
    }

    // Ventana listar
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, array(
                'label' => 'Rut'
            ))
            ->add('nombre')
            ->add('apellido')
            ->add('colegio', null, array(
                'label'=> 'Colegio'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Herramientas'
            ))

        ;
    }

    public function createQuery($context = 'list')
    {

        if ($context == 'list'){

            $query = parent::createQuery($context);
            $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');

            $role = $securityContext->isGranted("ROLE_SUPER_ADMIN");
            if ($role){
                return $query;
            }else{

                $query->andWhere(
                    $query->getRootAliases()[0] . '.roles LIKE :rol'
                );
                $query->setParameter('rol', '%ROLE_PROFESOR%');
                return $query;
            }
        }
    }

    // Pangtalla Ver
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, array(
                'label'=> "Rut"
            ))
            ->add('colegio', null, array(
                'label'=> 'Colegio'
            ))
            ->add('subsectors', null, array(
                'label' => 'Asignaturas'
            ))
        ;
    }

    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    // Rutas y botones
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('export') // Remueve el boton exportar
        ;

    }
}