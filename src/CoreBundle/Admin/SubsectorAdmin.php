<?php

namespace CoreBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SubsectorAdmin extends AbstractAdmin
{
    // Ventana Editar/Crear
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('nombre')
            ->add('alias');
    }


    // Configuración de Filtros
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('alias')
        ;
    }

    // Ventana listar
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('alias', null, array(
                'route'=> array('name'=>'show')
            ))
            ->add('nombre')
        ;
    }


    // Pangtalla Ver
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ->with('Usuarios',
            array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid box-success',
                'description' => 'Profesores con acceso a la asignatura'))

            ->add('usuarios', null, array('label'=>false,
                'route'=> array('name'=>'show')
            ))
            ->end()
        ->with('Niveles',
            array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid box-success',
                'description' => 'Niveles en que se dicta la asignatura'))
            ->add('nivels', null, array('label'=>false))
            ->end()
        ;
    }

    // Rutas y botones
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('export') // Remueve el boton exportar
        ;

    }


}