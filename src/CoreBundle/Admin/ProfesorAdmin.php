<?php

namespace CoreBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\CoreBundle\Validator\ErrorElement;

class ProfesorAdmin extends AbstractAdmin
{
    // Ventana Editar/Crear
    protected function configureFormFields(FormMapper $formMapper)
    {



        $formMapper
            ->with('Datos Generales', array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid box-primary',
                'description' => 'Datos generales del usuario'))

                ->add('username', 'text', array(
                    'label' => 'RUT'
                ))
                ->add('nombre', 'text', array(
                    'label' => 'Nombres'
                ))
                ->add('apellido', 'text', array(
                    'label' => 'Apellidos'
                ))
                ->add('email', 'text', array(
                    'label' => 'Email'
                ))
                ->add('email', 'email', array(
                    'label' => 'Email'
                ))
                ->add('colegio', null, array(
                    'label'=> 'Colegio'
                ))
            ->end();




    }

    public function prePersist($user)
    {
        $username = $user->getUsername();
        $password = substr($username,0,5);
        $user->setPlainPassword($password);
        $user->addRole("ROLE_PROFESOR");
        $this->getUserManager()->updateUser($user);
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('colegio')
            ->assertNotNull(array())
            ->end()
        ;
    }


    // Configuración de Filtros
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, array(
                'label'=> "Rut"
            ))
            ->add('colegio', null, array(
                'label'=> 'Colegio'
            ))
        ;
    }

    // Ventana listar profesores
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, array(
                'label' => 'Rut'
            ))
            ->add('nombre')
            ->add('colegio', null, array(
                'label'=> 'Colegio'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Herramientas'
            ))

        ;
    }

    // Pangtalla Ver Profesor
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, array(
                'label'=> "Rut"
            ))
            ->add('colegio', null, array(
                'label'=> 'Colegio'
            ))
        ;
    }


    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
}