<?php

namespace CoreBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ColegioAdmin extends AbstractAdmin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nombre', 'text', array(
                'label' => 'Colegio'
            ))
            ->add('rbd', 'text', array(
                'label' => 'RBD'
            ))
            ->add('director', 'text', array(
                'label' => 'Director'
            ))
            ->add('direccion', 'text', array(
                'label' => 'Dirección'
            ))
            ->add('telefono', 'text', array(
                'label' => 'Telefono'
            ))
            ->add('email', 'text', array(
                'label' => 'Email'
            ))
            ->add('comuna', 'entity', array(
                'label' => 'Comuna',
                'class'=> 'CoreBundle\Entity\Comuna'
            ))
            ->add('nivels', null, array(
                'label' => 'Niveles',
                'multiple'=>true,
                'by_reference'=> false
            ))


        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('comuna')
            ->add('rbd')
            ->add('email')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nombre')
            ->add('comuna')
            ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                'label' => 'Herramientas'
                ))

        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nombre')
            ->add('comuna')
            ->add('direccion')
            ->add('director')
            ->add('email')
            ->add('telefono')
            ->add('nivels')
        ;
    }


}