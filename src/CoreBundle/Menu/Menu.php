<?php

/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-06-17
 * Time: 21:48
 */

namespace CoreBundle\Menu;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Menu {

    public $us_dashboard = array('id' => 'dashboard',
        'name' => "Dashboard",
        'icon' => 'lnr-home',
        'active' => false,
        'submenu' => false);
    public $us_pruebas = array('id' => 'submenu1',
        'name' => "Pruebas",
        'icon' => 'lnr-apartment',
        'active' => false,
        'submenu' => true,
        'menu' => array(
            array(
                'id' => 'pruebas',
                'name' => "Mis Pruebas",
                'active' => false
            ),
            array(
                'id' => 'nueva_prueba',
                'name' => "Crear Prueba",
                'active' => false
            )
    ));
    public $us_preguntas = array('id' => 'submenu2',
        'name' => "Banco de preguntas",
        'icon' => 'lnr-cloud-sync',
        'active' => false,
        'submenu' => true,
        'menu' => array(
            array(
                'id' => 'banco_preguntas',
                'name' => "Banco de preguntas",
                'active' => false
            ),
            array(
                'id' => 'nueva_pregunta',
                'name' => "Crear Preguntas",
                'active' => false
            )
    ));

    public $so_soporte = array('id' => 'submenu3',
        'name' => "Soporte",
        'icon' => 'lnr lnr-cog',
        'active' => false,
        'submenu' => true,
        'menu' => array(
            array(
                'id' => 'ver_profesores',
                'name' => "Ver Profesores",
                'active' => false
            ),
            array(
                'id' => 'ver_asignaciones',
                'name' => "Ver asignaciones",
                'active' => false
            )
        ));

    public $sa_usuarios = array('id' => 'submenu_usuarios',
        'name' => "Usuarios",
        'icon' => 'lnr-user',
        'active' => false,
        'submenu' => true,
        'menu' => array(
            array(
                'id' => 'ver_usuarios',
                'name' => "Ver Usuarios",
                'active' => false
            ),
            array(
                'id' => 'crear_usuario',
                'name' => "Crear Usuario",
                'active' => false
            )
        ));
    public $sa_estadisticas = array('id' => 'estadisticas',
        'name' => "Estadisticas",
        'icon' => 'lnr-pie-chart',
        'active' => false,
        'submenu' => false);
    public $sa_comunicados = array('id' => 'submenu_comunicados',
        'name' => "Comunicados",
        'icon' => 'lnr-file-add',
        'active' => false,
        'submenu' => true,
        'menu' => array(
            array(
                'id' => 'ver_comunicados',
                'name' => "Ver Comunicados",
                'active' => false
            ),
            array(
                'id' => 'crear_comunicados',
                'name' => "Crear Comunicados",
                'active' => false
            )
        ));

    private $_container;
    private $_user;
    private $_logMenu;

    public function __construct(ContainerInterface $container, $logMenu) {
        $this->_container = $container;
        $this->_user = $this->_container->get('security.context')->getToken()->getUser();
        $this->_logMenu = $logMenu;
    }

    public function getMenu($id, $filter){
        $user = $this->getUserData();
        $log = new Logger('MenuUsuario');
        $log->pushHandler(new StreamHandler($this->_logMenu, Logger::INFO));
        $log->notice("Menu para $id");
        $log->notice("Usuario: " . $user['username']);

        $vars = get_object_vars($this);
        $items = array();

        foreach ($vars as $key => $item) {
            if ((substr($key, 0, 1) != '_') and (substr($key, 0, 3) == $filter)) {
                $items[$key] = $item;
            }
        }

        $response = array();

        foreach ($items as $key => $item) {
            if ($item['submenu']) {
                $new_sm = array();
                foreach ($item['menu'] as $nkey => $submenu) {
                    if ($id == $submenu['id']) {
                        $item['active'] = true;
                        $submenu['active'] = true;
                    }
                    array_push($new_sm, $submenu);
                }
                $item['menu'] = $new_sm;
            } else {
                if ($id == $item['id']) {
                    $item['active'] = true;
                }
            }
            array_push($response, $item);
        }

        $log->notice("Menu:", $response);
        return array('menu' => $response, 'user' => $user);
    }

    public function getMenuUsuario($id) {
        return $this->getMenu($id,'us_');
    }

    public function getMenuSuperAdmin($id) {
        return $this->getMenu($id, 'sa_');
    }

    public function getMenuSoporte($id) {
        return $this->getMenu($id, 'so_');
    }

    public function getUserData() {
        $response = array(
            'username' => $this->_user->getUsername(),
            'nombre_completo' => $this->_user->getUsernameCanonical()
        );
        return $response;
    }

}
