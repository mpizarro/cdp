<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-07-17
 * Time: 12:28
 */

namespace CoreBundle\Controller;

use CoreBundle\Entity\User;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request){
        $logger = $this->get('logger');

        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('crear_usuario');

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');


        $user_form = new User();
        $event = new GetResponseUserEvent($user_form, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }


        $form = $formFactory->createForm();
        $form->setData($user_form);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $role = $form->get('rol')->getData();
            $username = str_replace(['.','-'],'',$form->get('username')->getData());
            $old_user = $userManager->findUserByUsername($username);
            if($old_user instanceof User){
                $old_user->setEmail($user_form->getEmail());
                $old_user->getNombre($user_form->getNombre());
                $old_user->getApellido($user_form->getApellido());
                $userManager->updateUser($old_user);
                return new Response(json_encode(array('status' => true,'msg'=>'Usuario Actualizado')));
            }else {
                if ($form->isValid()) {
                    $user = $user_form;
                    $user->setUsername(str_replace(['.','-'],'',$user->getUsername()));
                    $user->setEnabled(true);
                    $user->addRole($role);
                    if ($role == 'ROLE_SOPORTE') {
                        $user->addRole('ROLE_PROFESOR');
                    }
                    $userManager->updateUser($user);
                    return new Response(json_encode(array('status' => true, 'msg'=>'Usuario Creado')));
                } else {
                    return new Response(json_encode(array('status' => false, 'error' => $this->getErrorMessages($form))));
                }
            }
        }

        return $this->render('SuperAdminBundle::crear_usuario.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
                'form'=>$form->createView()
            ));
    }

    private function getErrorMessages(Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}