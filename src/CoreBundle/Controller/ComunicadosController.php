<?php

namespace CoreBundle\Controller;

use CoreBundle\Entity\Comunicados;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Comunicado controller.
 *
 */
class ComunicadosController extends Controller
{
    /**
     * Lists all comunicado entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $comunicados = $em->getRepository('CoreBundle:Comunicados')->findAll();
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('ver_comunicados');

        return $this->render('SuperAdminBundle::ver_comunicados.html.twig',
            array('menu'=> $menu['menu'],
                'user'=>$menu['user'],
                'comunicados' => $comunicados
            ));

    }

    /**
     * Creates a new comunicado entity.
     *
     */
    public function newAction(Request $request)
    {
        $creator_menu = $this->container->get('user.menu.left');
        $menu = $creator_menu->getMenuSuperAdmin('crear_comunicados');
        $user = $this->getUser();

        $comunicado = new Comunicados();
        $form = $this->createForm('CoreBundle\Form\ComunicadosType', $comunicado);
        $form->handleRequest($request);
        $comunicado->setUsers(array('ROLE_PROFESORES'));
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('logger')->info($user->getId());
            $time = str_replace('/','-', $form->get('sendOn')->getData());
            $date = date_create_from_format('d-m-Y H:i', $time);
            $em = $this->getDoctrine()->getManager();

            $comunicado->setAuthor($user);
            $comunicado->setSendOn($date);

            $em->persist($comunicado);
            $em->flush();

            return $this->redirectToRoute('super-admin_comunicados_show', array('id' => $comunicado->getId()));
        }

        return $this->render('SuperAdminBundle::crear_comunicado.html.twig',
            array('menu'=> $menu['menu'],
                'comunicado' => $comunicado,
                'user'=>$menu['user'],
                'form'=>$form->createView()
            ));
    }

    /**
     * Finds and displays a comunicado entity.
     *
     */
    public function showAction(Comunicados $comunicado)
    {
        $deleteForm = $this->createDeleteForm($comunicado);

        return $this->render('comunicados/show.html.twig', array(
            'comunicado' => $comunicado,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing comunicado entity.
     *
     */
    public function editAction(Request $request, Comunicados $comunicado)
    {
        $deleteForm = $this->createDeleteForm($comunicado);
        $editForm = $this->createForm('CoreBundle\Form\ComunicadosType', $comunicado);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('super-admin_comunicados_edit', array('id' => $comunicado->getId()));
        }

        return $this->render('comunicados/edit.html.twig', array(
            'comunicado' => $comunicado,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a comunicado entity.
     *
     */
    public function deleteAction(Request $request, Comunicados $comunicado)
    {
        $form = $this->createDeleteForm($comunicado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comunicado);
            $em->flush();
        }

        return $this->redirectToRoute('super-admin_comunicados_index');
    }

    /**
     * Creates a form to delete a comunicado entity.
     *
     * @param Comunicados $comunicado The comunicado entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comunicados $comunicado)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('super-admin_comunicados_delete', array('id' => $comunicado->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
