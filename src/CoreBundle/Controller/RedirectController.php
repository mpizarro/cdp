<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 10-07-17
 * Time: 12:38
 */

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RedirectController extends  Controller
{
    public function indexAction(){
        $authChecker = $this->container->get('security.authorization_checker');
        $router = $this->container->get('router');

        if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
            return new RedirectResponse($router->generate('sonata_admin_dashboard'), 307);
        }

        if ($authChecker->isGranted('ROLE_SOPORTE')) {
            return new RedirectResponse($router->generate('sonata_admin_dashboard'), 307);
        }

        if ($authChecker->isGranted('ROLE_PROFESOR')) {
            return new RedirectResponse($router->generate('sonata_admin_dashboard'), 307);
        }

        if ($authChecker->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            return new RedirectResponse($router->generate('fos_user_security_login'), 307);
        }
    }

    public function profileAction(){
        $user = $this->getUser();
        return $this->render('CoreBundle::profile.html.twig',
            array('user' =>$user
            ));
    }


}
