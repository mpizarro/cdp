<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 24-08-17
 * Time: 18:39
 */

namespace CoreBundle\Controller;


use CoreBundle\Entity\Eje;
use CoreBundle\Entity\Habilidad;
use CoreBundle\Entity\Nivel;
use CoreBundle\Entity\Pregunta;
use CoreBundle\Entity\Subsector;
use CoreBundle\Entity\Taxonomia;
use CoreBundle\Entity\TipoPrueba;
use CoreBundle\Form\PreguntaType;
use Sonata\AdminBundle\Controller\CRUDController as Controller;



class CreatePreguntaController extends Controller
{

    public function createAction()
    {
        $view = array();
        $user = $this->getUser();
        $pregunta = new Pregunta();
        $em = $this->getDoctrine()->getManager();
        $log = $this->get('logger');

        $form = $this->createForm(PreguntaType::class, $pregunta);

        $form->handleRequest($this->get('request'));

        if ($form->isSubmitted())
        {
            $simils = $em->getRepository(Pregunta::class)->findBy(
                array(
                    'subsector'=>$pregunta->getSubsector(),
                    'nivel'=>$pregunta->getNivel(),
                    'tipoPrueba'=>$pregunta->getTipoPrueba()
                )
            );

            $codigo = $pregunta->getSubsector()->getAlias()."-"
                .$pregunta->getNivel()->getAlias()."-"
                .substr($pregunta->getTipoPrueba()->getNombre(),0,3)."-"
                .(count($simils)+1);
            $pregunta->setCodigo($codigo);
            $pregunta->setTitulo($codigo);
            $pregunta->setUsuario($user);

            $em->persist($pregunta);
            $em->flush();
            return $this->redirect($this->generateUrl(
                'admin_core_pregunta_show',
                array('id'=>$pregunta->getId()))
            );
        }


        $view['tipo_prueba'] = $em->getRepository(TipoPrueba::class)->findAll();

        if ($user->getColegio() != null){
            $niveles = $user->getColegio()->getNivels();
            $view['nivel']['basica'] = array();
            $view['nivel']['media'] = array();

            foreach ($niveles as $nivel) {
                if (strpos($nivel->getNivel(),'Basico') !== false){
                    $view['nivel']['basica'][]=$nivel;
                }else{
                    $view['nivel']['media'][]=$nivel;
                }
            }
        }else{
            $view['nivel']['basica'] = $em->getRepository(Nivel::class)
                ->createQueryBuilder('nivel')
                ->where("nivel.nivel LIKE '%Basico%'")
                ->orderBy('nivel.id')
                ->getQuery()->getResult();
            ;
            $view['nivel']['media'] = $em->getRepository(Nivel::class)
                ->createQueryBuilder('nivel')
                ->where("nivel.nivel LIKE '%Medio%'")
                ->orderBy('nivel.id')
                ->getQuery()->getResult();
            ;
        }

        if ($user->getSubsectors() != null){
            $view['subsectors'] = $user->getSubsectors();
            $log->warning("Con Subs");
        }else{
            $log->warning("Sin Subs");
            $view['subsectors'] = $em->getRepository(Subsector::class)->findAll();
        }

        $view['habilidades'] = $em->getRepository(Habilidad::class)->findAll();
        $view['ejes'] = $em->getRepository(Eje::class)->findAll();
        $view['taxonomias'] = $em->getRepository(Taxonomia::class)->findAll();
        $view['form'] = $form->createView();



        return $this->render('@Core/Admin/preguntas_create.html.twig', $view);
    }



}