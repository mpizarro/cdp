<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pregunta
 *
 * @ORM\Table(name="pregunta")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\PreguntaRepository")
 */
class Pregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="string", length=1024, nullable=true)
     */
    private $contenido;


    /**
     * @ORM\Column(name="padre", type="integer", options={"default" : null}, nullable=true)
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Pregunta")
     * @ORM\JoinColumn(name="padre_id", referencedColumnName="id")
     */
    private $padre;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Taxonomia")
     * @ORM\JoinColumn(name="taxonomia_id", referencedColumnName="id", nullable=true)
     */
    private $taxonomia;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Eje")
     * @ORM\JoinColumn(name="eje_id", referencedColumnName="id")
     */
    private $eje;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Dificultad")
     * @ORM\JoinColumn(name="dificultad_id", referencedColumnName="id")
     */
    private $dificultad;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Habilidad")
     * @ORM\JoinColumn(name="habilidad_id", referencedColumnName="id")
     */
    private $habilidad;


    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Nivel")
     * @ORM\JoinColumn(name="nivel_id", referencedColumnName="id")
     */
    private $nivel;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Subsector")
     * @ORM\JoinColumn(name="subsector_id", referencedColumnName="id")
     */
    private $subsector;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Prueba", mappedBy="pregunts")
     */
    private $prueba;

    /**
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private $publicado;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\TipoPrueba")
     * @ORM\JoinColumn(name="tipo_prueba", referencedColumnName="id")
     */
    private $tipoPrueba;

    public function __toString()
    {
        return (string) $this->getCodigo();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Pregunta
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Pregunta
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return Pregunta
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set padre
     *
     * @param \CoreBundle\Entity\Pregunta $padre
     *
     * @return Pregunta
     */
    public function setPadre(\CoreBundle\Entity\Pregunta $padre = null)
    {
        $this->padre = $padre;

        return $this;
    }

    /**
     * Get padre
     *
     * @return \CoreBundle\Entity\Pregunta
     */
    public function getPadre()
    {
        return $this->padre;
    }

    /**
     * Set taxonomia
     *
     * @param \CoreBundle\Entity\Taxonomia $taxonomia
     *
     * @return Pregunta
     */
    public function setTaxonomia(\CoreBundle\Entity\Taxonomia $taxonomia = null)
    {
        $this->taxonomia = $taxonomia;

        return $this;
    }

    /**
     * Get taxonomia
     *
     * @return \CoreBundle\Entity\Taxonomia
     */
    public function getTaxonomia()
    {
        return $this->taxonomia;
    }

    /**
     * Set eje
     *
     * @param \CoreBundle\Entity\Eje $eje
     *
     * @return Pregunta
     */
    public function setEje(\CoreBundle\Entity\Eje $eje = null)
    {
        $this->eje = $eje;

        return $this;
    }

    /**
     * Get eje
     *
     * @return \CoreBundle\Entity\Eje
     */
    public function getEje()
    {
        return $this->eje;
    }

    /**
     * Set dificultad
     *
     * @param \CoreBundle\Entity\Dificultad $dificultad
     *
     * @return Pregunta
     */
    public function setDificultad(\CoreBundle\Entity\Dificultad $dificultad = null)
    {
        $this->dificultad = $dificultad;

        return $this;
    }

    /**
     * Get dificultad
     *
     * @return \CoreBundle\Entity\Dificultad
     */
    public function getDificultad()
    {
        return $this->dificultad;
    }

    /**
     * Set habilidad
     *
     * @param \CoreBundle\Entity\Habilidad $habilidad
     *
     * @return Pregunta
     */
    public function setHabilidad(\CoreBundle\Entity\Habilidad $habilidad = null)
    {
        $this->habilidad = $habilidad;

        return $this;
    }

    /**
     * Get habilidad
     *
     * @return \CoreBundle\Entity\Habilidad
     */
    public function getHabilidad()
    {
        return $this->habilidad;
    }

    /**
     * Set nivel
     *
     * @param \CoreBundle\Entity\Nivel $nivel
     *
     * @return Pregunta
     */
    public function setNivel(\CoreBundle\Entity\Nivel $nivel = null)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return \CoreBundle\Entity\Nivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set subsector
     *
     * @param \CoreBundle\Entity\Subsector $subsector
     *
     * @return Pregunta
     */
    public function setSubsector(\CoreBundle\Entity\Subsector $subsector = null)
    {
        $this->subsector = $subsector;

        return $this;
    }

    /**
     * Get subsector
     *
     * @return \CoreBundle\Entity\Subsector
     */
    public function getSubsector()
    {
        return $this->subsector;
    }

    /**
     * Set usuario
     *
     * @param \CoreBundle\Entity\User $usuario
     *
     * @return Pregunta
     */
    public function setUsuario(\CoreBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \CoreBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set publicado
     *
     * @param boolean $publicado
     *
     * @return Pregunta
     */
    public function setPublicado($publicado)
    {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado
     *
     * @return boolean
     */
    public function getPublicado()
    {
        return $this->publicado;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prueba = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prueba
     *
     * @param \CoreBundle\Entity\Prueba $prueba
     *
     * @return Pregunta
     */
    public function addPrueba(\CoreBundle\Entity\Prueba $prueba)
    {
        $this->prueba[] = $prueba;

        return $this;
    }

    /**
     * Remove prueba
     *
     * @param \CoreBundle\Entity\Prueba $prueba
     */
    public function removePrueba(\CoreBundle\Entity\Prueba $prueba)
    {
        $this->prueba->removeElement($prueba);
    }

    /**
     * Get prueba
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrueba()
    {
        return $this->prueba;
    }

    /**
     * Set tipoPrueba
     *
     * @param \CoreBundle\Entity\TipoPrueba $tipoPrueba
     *
     * @return Pregunta
     */
    public function setTipoPrueba(\CoreBundle\Entity\TipoPrueba $tipoPrueba = null)
    {
        $this->tipoPrueba = $tipoPrueba;

        return $this;
    }

    /**
     * Get tipoPrueba
     *
     * @return \CoreBundle\Entity\TipoPrueba
     */
    public function getTipoPrueba()
    {
        return $this->tipoPrueba;
    }
}
