<?php

namespace CoreBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Colegio
 *
 * @ORM\Table(name="colegio")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\ColegioRepository")
 */
class Colegio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="rbd", type="string", length=255, unique=true)
     */
    private $rbd;

    /**
     * @var string
     *
     * @ORM\Column(name="director", type="string", length=1024, nullable=true)
     */
    private $director;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=1024, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=1024, nullable=true)
     */
    private $email;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Comuna")
     * @ORM\JoinColumn(name="comuna_id", referencedColumnName="id")
     */
    private $comuna;

    /**
     * @ORM\OneToMany(targetEntity="CoreBundle\Entity\User", mappedBy="colegio")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuarios;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Nivel", inversedBy="colegios")
     * @ORM\JoinTable(name="nivel_has_colegio")
     * @ORM\OrderBy({"id"="ASC"})
     */
    private $nivels;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Colegio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set rbd
     *
     * @param string $rbd
     *
     * @return Colegio
     */
    public function setRbd($rbd)
    {
        $this->rbd = $rbd;

        return $this;
    }

    /**
     * Get rbd
     *
     * @return string
     */
    public function getRbd()
    {
        return $this->rbd;
    }

    /**
     * Set director
     *
     * @param string $director
     *
     * @return Colegio
     */
    public function setDirector($director)
    {
        $this->director = $director;

        return $this;
    }

    /**
     * Get director
     *
     * @return string
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Colegio
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Colegio
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Colegio
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comuna
     *
     * @param string comuna
     *
     * @return Colegio
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;

        return $this;
    }

    /**
     * Get comuna
     *
     * @return integer
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getNombre();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add usuario
     *
     * @param \CoreBundle\Entity\User $usuario
     *
     * @return Colegio
     */
    public function addUsuario(\CoreBundle\Entity\User $usuario)
    {
        $this->usuarios[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \CoreBundle\Entity\User $usuario
     */
    public function removeUsuario(\CoreBundle\Entity\User $usuario)
    {
        $this->usuarios->removeElement($usuario);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }


    /**
     * Add nivel
     *
     * @param \CoreBundle\Entity\Nivel $nivel
     *
     * @return Colegio
     */
    public function addNivel(\CoreBundle\Entity\Nivel $nivel)
    {
        $this->nivels[] = $nivel;

        return $this;
    }

    /**
     * Remove nivel
     *
     * @param \CoreBundle\Entity\Nivel $nivel
     */
    public function removeNivel(\CoreBundle\Entity\Nivel $nivel)
    {
        $this->nivels->removeElement($nivel);
    }

    /**
     * Get nivels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNivels()
    {

        return $this->nivels;
    }
}
