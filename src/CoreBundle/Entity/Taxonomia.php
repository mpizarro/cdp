<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taxonomia
 *
 * @ORM\Table(name="taxonomia")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\TaxonomiaRepository")
 */
class Taxonomia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="taxonomia", type="string", length=45, unique=true)
     */
    private $taxonomia;


    public function __toString()
    {
        return $this->getTaxonomia();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taxonomia
     *
     * @param string $taxonomia
     *
     * @return Taxonomia
     */
    public function setTaxonomia($taxonomia)
    {
        $this->taxonomia = $taxonomia;

        return $this;
    }

    /**
     * Get taxonomia
     *
     * @return string
     */
    public function getTaxonomia()
    {
        return $this->taxonomia;
    }
}

