<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comunicados
 *
 * @ORM\Table(name="comunicados")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\ComunicadosRepository")
 */
class Comunicados
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var array
     *
     * @ORM\Column(name="users", type="array")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_on", type="datetime")
     */
    private $sendOn;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Comunicados
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set users
     *
     * @param array $users
     * @return Comunicados
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return array 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set author
     *
     * @param integer $author
     * @return Comunicados
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return integer 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set sendOn
     *
     * @param \DateTime $sendOn
     * @return Comunicados
     */
    public function setSendOn($sendOn)
    {
        $this->sendOn = $sendOn;

        return $this;
    }

    /**
     * Get sendOn
     *
     * @return \DateTime 
     */
    public function getSendOn()
    {
        return $this->sendOn;
    }
}
