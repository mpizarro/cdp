<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subsector
 *
 * @ORM\Table(name="subsector")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\SubsectorRepository")
 */
class Subsector
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, unique=true)
     */
    private $alias;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\User", mappedBy="subsectors")
     */
    private $usuarios;


    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Nivel", mappedBy="subsectors")
     */
    private $nivels;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Subsector
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Subsector
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Get class to string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add usuario
     *
     * @param \CoreBundle\Entity\User $usuario
     *
     * @return Subsector
     */
    public function addUsuario(\CoreBundle\Entity\User $usuario)
    {
        $this->usuarios[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \CoreBundle\Entity\User $usuario
     */
    public function removeUsuario(\CoreBundle\Entity\User $usuario)
    {
        $this->usuarios->removeElement($usuario);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }


    /**
     * Add nivel
     *
     * @param \CoreBundle\Entity\Nivel $nivel
     *
     * @return Subsector
     */
    public function addNivel(\CoreBundle\Entity\Nivel $nivel)
    {
        $this->nivels[] = $nivel;

        return $this;
    }

    /**
     * Remove nivel
     *
     * @param \CoreBundle\Entity\Nivel $nivel
     */
    public function removeNivel(\CoreBundle\Entity\Nivel $nivel)
    {
        $this->nivels->removeElement($nivel);
    }

    /**
     * Get nivels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNivels()
    {
        return $this->nivels;
    }
}
