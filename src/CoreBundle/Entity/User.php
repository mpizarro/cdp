<?php
/**
 * Created by PhpStorm.
 * User: jreyes
 * Date: 12-06-17
 * Time: 15:51
 */

namespace CoreBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",options={"default" : "Sin Nombre"})
     */

    protected $nombre;

    /**
     * @ORM\Column(type="string",options={"default" : "Sin Apellido"})
     */

    protected $apellido;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Colegio", inversedBy="usuarios")
     * @ORM\JoinColumn(name="colegio_id", referencedColumnName="id")
     */

    protected $colegio;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Subsector", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuario_has_subsector")
     */
    protected $subsectors;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fono;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::setEnabled(true);
        $this->subsectors = new ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return User
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set colegio
     *
     * @param \CoreBundle\Entity\Colegio $colegio
     *
     * @return User
     */
    public function setColegio(\CoreBundle\Entity\Colegio $colegio = null)
    {
        $this->colegio = $colegio;

        return $this;
    }

    /**
     * Get colegio
     *
     * @return \CoreBundle\Entity\Colegio
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    /**
     * Get Last Login String
     *
     * return string
     */
    public function getLastLoginString()
    {
        $last_login = parent::getLastLogin();
        return $last_login->format('H:i:s d/m/Y');
    }



    /**
     * Add subsector
     *
     * @param \CoreBundle\Entity\Subsector $subsector
     *
     * @return User
     */
    public function addSubsector(\CoreBundle\Entity\Subsector $subsector)
    {
        $this->subsectors[] = $subsector;

        return $this;
    }

    /**
     * Remove subsector
     *
     * @param \CoreBundle\Entity\Subsector $subsector
     */
    public function removeSubsector(\CoreBundle\Entity\Subsector $subsector)
    {
        $this->subsectors->removeElement($subsector);
    }

    /**
     * Get subsectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubsectors()
    {
        return $this->subsectors;
    }

    /**
     * Set fono
     *
     * @param string $fono
     *
     * @return User
     */
    public function setFono($fono)
    {
        $this->fono = $fono;

        return $this;
    }

    /**
     * Get fono
     *
     * @return string
     */
    public function getFono()
    {
        return $this->fono;
    }
}
