<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eje
 *
 * @ORM\Table(name="eje")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\EjeRepository")
 */
class Eje
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="eje", type="string", length=45, unique=true)
     */
    private $eje;


    public function __toString()
    {
        return (string) $this->getEje();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eje
     *
     * @param string $eje
     *
     * @return Eje
     */
    public function setEje($eje)
    {
        $this->eje = $eje;

        return $this;
    }

    /**
     * Get eje
     *
     * @return string
     */
    public function getEje()
    {
        return $this->eje;
    }
}

