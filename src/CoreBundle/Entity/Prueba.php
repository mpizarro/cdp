<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prueba
 *
 * @ORM\Table(name="prueba")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\PruebaRepository")
 */
class Prueba
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=1024, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Pregunta", inversedBy="prueba")
     * @ORM\JoinTable(name="pregunta_has_prueba")
     */
    private $pregunts;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\TipoPrueba", inversedBy="pruebas")
     * @ORM\JoinColumn(name="tipo_prueba", referencedColumnName="id")
     */
    private $tipoPrueba;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Prueba
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Prueba
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pregunts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pregunt
     *
     * @param \CoreBundle\Entity\Pregunta $pregunt
     *
     * @return Prueba
     */
    public function addPregunt(\CoreBundle\Entity\Pregunta $pregunt)
    {
        $this->pregunts[] = $pregunt;

        return $this;
    }

    /**
     * Remove pregunt
     *
     * @param \CoreBundle\Entity\Pregunta $pregunt
     */
    public function removePregunt(\CoreBundle\Entity\Pregunta $pregunt)
    {
        $this->pregunts->removeElement($pregunt);
    }

    /**
     * Get pregunts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPregunts()
    {
        return $this->pregunts;
    }

    /**
     * Set tipoPrueba
     *
     * @param \CoreBundle\Entity\TipoPrueba $tipoPrueba
     *
     * @return Prueba
     */
    public function setTipoPrueba(\CoreBundle\Entity\TipoPrueba $tipoPrueba = null)
    {
        $this->tipoPrueba = $tipoPrueba;

        return $this;
    }

    /**
     * Get tipoPrueba
     *
     * @return \CoreBundle\Entity\TipoPrueba
     */
    public function getTipoPrueba()
    {
        return $this->tipoPrueba;
    }
}
