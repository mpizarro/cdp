<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Habilidad
 *
 * @ORM\Table(name="habilidad")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\HabilidadRepository")
 */
class Habilidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="habilidad", type="string", length=100)
     */
    private $habilidad;


    public function __toString()
    {
        return $this->getHabilidad();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set habilidad
     *
     * @param string $habilidad
     *
     * @return Habilidad
     */
    public function setHabilidad($habilidad)
    {
        $this->habilidad = $habilidad;

        return $this;
    }

    /**
     * Get habilidad
     *
     * @return string
     */
    public function getHabilidad()
    {
        return $this->habilidad;
    }
}

