<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nivel
 *
 * @ORM\Table(name="nivel")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\NivelRepository")
 */
class Nivel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel", type="string", length=45, unique=true)
     */
    private $nivel;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=45, unique=true, nullable=true)
     */
    private $alias;

    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Subsector", inversedBy="niveles")
     * @ORM\JoinTable(name="nivel_has_subsector")
     */
    private $subsectors;


    /**
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Colegio", mappedBy="niveles")
     */
    private $colegios;


    public function __toString()
    {
        return $this->getNivel();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return Nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subsectors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add subsector
     *
     * @param \CoreBundle\Entity\Subsector $subsector
     *
     * @return Nivel
     */
    public function addSubsector(\CoreBundle\Entity\Subsector $subsector)
    {
        $this->subsectors[] = $subsector;

        return $this;
    }

    /**
     * Remove subsector
     *
     * @param \CoreBundle\Entity\Subsector $subsector
     */
    public function removeSubsector(\CoreBundle\Entity\Subsector $subsector)
    {
        $this->subsectors->removeElement($subsector);
    }

    /**
     * Get subsectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubsectors()
    {
        return $this->subsectors;
    }

    /**
     * Add colegio
     *
     * @param \CoreBundle\Entity\Colegio $colegio
     *
     * @return Nivel
     */
    public function addColegio(\CoreBundle\Entity\Colegio $colegio)
    {
        $this->colegios[] = $colegio;

        return $this;
    }

    /**
     * Remove colegio
     *
     * @param \CoreBundle\Entity\Colegio $colegio
     */
    public function removeColegio(\CoreBundle\Entity\Colegio $colegio)
    {
        $this->colegios->removeElement($colegio);
    }

    /**
     * Get colegios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColegios()
    {
        return $this->colegios;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Nivel
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
