<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Respuesta
 *
 * @ORM\Table(name="respuesta")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\RespuestaRepository")
 */
class Respuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="string", length=1024)
     */
    private $contenido;

    /**
     * @var bool
     *
     * @ORM\Column(name="correcta", type="boolean")
     */
    private $correcta;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Pregunta")
     * @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id")
     */
    private $pregunta;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return Respuesta
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set correcta
     *
     * @param boolean $correcta
     *
     * @return Respuesta
     */
    public function setCorrecta($correcta)
    {
        $this->correcta = $correcta;

        return $this;
    }

    /**
     * Get correcta
     *
     * @return bool
     */
    public function getCorrecta()
    {
        return $this->correcta;
    }

    /**
     * Set pregunta
     *
     * @param \CoreBundle\Entity\Pregunta $pregunta
     *
     * @return Respuesta
     */
    public function setPregunta(\CoreBundle\Entity\Pregunta $pregunta = null)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return \CoreBundle\Entity\Pregunta
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }
}
