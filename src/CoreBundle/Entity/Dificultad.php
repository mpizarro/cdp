<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dificultad
 *
 * @ORM\Table(name="dificultad")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\DificultadRepository")
 */
class Dificultad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dificultad", type="string", length=20, unique=true)
     */
    private $dificultad;


    public function __toString()
    {
        return $this->getDificultad();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dificultad
     *
     * @param string $dificultad
     *
     * @return Dificultad
     */
    public function setDificultad($dificultad)
    {
        $this->dificultad = $dificultad;

        return $this;
    }

    /**
     * Get dificultad
     *
     * @return string
     */
    public function getDificultad()
    {
        return $this->dificultad;
    }
}

